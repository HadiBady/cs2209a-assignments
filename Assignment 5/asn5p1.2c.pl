%  Developed by M. Hadi Badawi

% nQueens checks to see if the Board configuration is a valid
% nQueen's solution
nQueens(Board, N):-
        % generating a list that is as long as Board
        numlist(1,N,List),
        % permutation checks so that no queens are vertically or horizontally aligned
        permutation(List,Board),
        % this checks for diagonals to make sure no queens are diagonally aligned
        checkDiagonals(Board, N).


% the checkDiagonals uses the helper method checkDiagonalHelper method
% the notIdentical method

checkDiagonals(Board, N):-
        % generating a list that is as long as Board
        numlist(1,N,List),
        checkDiagonalHelper(List, Board, Sum, Dif),
        notIdentical(Sum),
        notIdentical(Dif).


%  this program has the checkDiagonals Function which determines
%  whether or not that a certain solution is true or not
% the checkDiagonalHelper checks the 2 potential Diagonals, a potential
% diagonal going upwards, and the potnetial diagonal going downwards
%
% the upwards diagonal like this | the downwards diagonal like this
%    1  2  3  4                  |         1  2  3  4
%  -------------                 |         --------------
% 1|  |Q  |  |  |                |        1|  |   |  |  |
% 2|Q |   |  |  |                |        2|  |   |  |  |
% 3|  |   |  |  |                |        3|  |   |Q |  |
% 4|  |   |  |  |                |        4|  |   |  |Q |
%  -------------                 |         --------------
%
% for the upwards diagonal, if you take the addition of column and row,
% you will see that 2 + 1 = 1 + 2, thus we end up with 3, which means
% one way of detecting if something is in diagonal, is to add the row
% and column and see if it is equal to another queen
%
% the other way to detect a diagonal, which is the downwards diagonal
% that if you take the difference of row and col, like 3 - 3 and 4 - 4,
% which results in the answer of 0. So this other way is when the
% difference between the row and column of one queen is equal to the
% difference of another queen.
checkDiagonalHelper([Row1|Row], [Col1| Col], [Sum1| SumR], [Dif1| DifR]) :-
     Sum1 is Row1 + Col1,
     Dif1 is Row1 - Col1,
     checkDiagonalHelper(Row, Col, SumR, DifR).

% base case for the recursion, to stop when all the lists are empty
checkDiagonalHelper([],[],[],[]).

% the notIdentical makes sure that no two items in a list are equal
% hence, no identical values in the list
notIdentical([Head|Tail]) :-  \+member(Head, Tail), notIdentical(Tail).
% the base case, which is when there is only 1 time in the list
notIdentical([T]).





